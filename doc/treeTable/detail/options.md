<table class="layui-table">
  <colgroup>
    <col width="150">
    <col>
    <col width="100">
    <col width="100">
  </colgroup>
  <thead>
    <tr>
      <th>属性名</th>
      <th>描述</th>
      <th>类型</th>
      <th>默认值</th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td>tree</td>
      <td>树节点的主要配置信息</td>
      <td>{}</td>
      <td></td>
    </tr>
    <tr>
      <td>tree.data.key.children</td>
      <td>子节点的属性名称</td>
      <td>String</td>
      <td>children</td>
    </tr>
    <tr>
      <td>tree.data.key.isParent</td>
      <td>节点是否为父节点的属性名称</td>
      <td>String</td>
      <td>isParent</td>
    </tr>
    <tr>
      <td>tree.data.key.name</td>
      <td>节点名称的属性名称</td>
      <td>String</td>
      <td>name</td>
    </tr>
    <tr>
      <td>tree.data.simpleData.enable</td>
      <td>是否简单数据模式</td>
      <td>Boolean</td>
      <td>false</td>
    </tr>
    <tr>
      <td>tree.data.simpleData.idKey</td>
      <td>唯一标识的属性名称</td>
      <td>String</td>
      <td>id</td>
    </tr>
    <tr>
      <td>tree.data.simpleData.pIdKey</td>
      <td>父节点唯一标识的属性名称</td>
      <td>String</td>
      <td>pId</td>
    </tr>
    <tr>
      <td>tree.data.simpleData.rootPId</td>
      <td>根节点唯一标识属性值</td>
      <td>String</td>
      <td>null</td>
    </tr>
    <tr>
      <td>tree.view.indent</td>
      <td>层级缩进量</td>
      <td>Int</td>
      <td>14</td>
    </tr>
    <tr>
      <td>tree.view.flexIconClose</td>
      <td>关闭时候的折叠图标</td>
      <td>String</td>
      <td>&lt;i class="layui-icon layui-icon-triangle-r">&lt;/i></td>
    </tr>
    <tr>
      <td>tree.view.flexIconOpen</td>
      <td>打开时候的折叠图标</td>
      <td>String</td>
      <td>&lt;i class="layui-icon layui-icon-triangle-d">&lt;/i></td>
    </tr>
    <tr>
      <td>tree.view.showIcon</td>
      <td>是否显示节点的图标</td>
      <td>Boolean</td>
      <td>true</td>
    </tr>
    <tr>
      <td>tree.view.icon</td>
      <td>节点图标，如果设置了这个属性或者数据中有这个字段信息，不管打开还是关闭都以这个图标的值为准</td>
      <td>String</td>
      <td></td>
    </tr>
    <tr>
      <td>tree.view.iconClose</td>
      <td>打开时候的图标</td>
      <td>String</td>
      <td>&lt;i class="layui-icon layui-icon-folderOpen">&lt;/i></td>
    </tr>
    <tr>
      <td>tree.view.iconOpen</td>
      <td>关闭时候的图标</td>
      <td>String</td>
      <td>&lt;i class="layui-icon layui-icon-folder">&lt;/i></td>
    </tr>
    <tr>
      <td>tree.view.iconLeaf</td>
      <td>叶子节点的图标</td>
      <td>String</td>
      <td>&lt;i class="layui-icon layui-icon-file">&lt;/i></td>
    </tr>
    <tr>
      <td>tree.view.showFlexIconIfNotParent</td>
      <td>当节点不是父节点的时候是否显示折叠图标</td>
      <td>Boolean</td>
      <td>false</td>
    </tr>
    <tr>
      <td>tree.view.dblClickExpand</td>
      <td>双击节点时，是否自动展开父节点的标识</td>
      <td>Boolean</td>
      <td>true</td>
    </tr>
    <tr>
      <td>tree.async.enable</td>
      <td>是否开启异步加载模式， 只有开启时async的其他属性配置才会起作用。
注意：异步加载子节点不应该跟simpleData同时开启，可以是url+simpleData的方式，获取完整的简单数据进行转换，但是如果开启了异步加载模式一般来说就是分层按需加载某个父节点下的子节点，不存在返回的数据是带层级的简单数据结构的数据。</td>
      <td>Boolean</td>
      <td>false</td>
    </tr>
    <tr>
      <td>tree.async.url</td>
      <td>异步加载的接口，可以根据需要设置与顶层接口不同的接口，如果相同可以不设置该参数</td>
      <td>String</td>
      <td></td>
    </tr>
    <tr>
      <td>tree.async.type</td>
      <td>请求的接口类型，设置可缺省同上</td>
      <td>String</td>
      <td></td>
    </tr>
    <tr>
      <td>tree.async.contentType</td>
      <td>提交参数的数据类型，设置可缺省同上</td>
      <td>String</td>
      <td></td>
    </tr>
    <tr>
      <td>tree.async.headers</td>
      <td>提交请求头，设置可缺省同上</td>
      <td>Object</td>
      <td>{}</td>
    </tr>
    <tr>
      <td>tree.async.where</td>
      <td>提交参数的数据，设置可缺省同上</td>
      <td>Object</td>
      <td>{}</td>
    </tr>
    <tr>
      <td>tree.async.autoParam</td>
      <td>自动参数，可以根据配置项以及当前节点的数据传参，比如['type', 'age=age', 'pId=id']，请求参数将包含{type: 父节点type, age: 父节点age, pId: 父节点id}</td>
      <td>Array</td>
      <td>[]</td>
    </tr>
    <tr>
      <td>tree.callback.beforeExpand</td>
      <td>展开前回调，可以在展开或者关闭之前调用，传入当前表格id，当前操作的行数据以及要展开或关闭的状态，如果回调返回false则取消该次操作。</td>
      <td>function(tableId, trData, expandFlag)</td>
      <td></td>
    </tr>
    <tr>
      <td>tree.callback.onExpand</td>
      <td>展开或关闭后的回调，参数和beforeExpand一样</td>
      <td>function(tableId, trData, expandFlag)</td>
      <td></td>
    </tr>
  </tbody>
</table>
