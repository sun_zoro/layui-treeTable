<h3 lay-toc="{level: 2, id: 'examples'}" class="layui-hide">简单使用</h3>


<h3 id="demo-treeData" lay-toc="{level: 2}" class="ws-bold">简单运用</h3>

将树形的data渲染成树表

<pre class="layui-code" lay-options="{preview: true, style: 'height: 335px; overflow: auto;', layout: ['preview', 'code'], tools: ['full']}">
  <textarea>
{{- d.include('docs/treeTable/examples/treeData.md') }}
  </textarea>
</pre>



