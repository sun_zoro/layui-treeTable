---
title: 树表组件 treeTable
toc: true
---

# 树表组件

> 树表组件 `treeTable` 是在 `table` 组件基础上的一个拓展组件，直接父节点展开关闭，异步子节点加载等常见树控件功能

<h2 id="examples" lay-toc="{hot: true}" style="margin-bottom: 0;">示例</h2>

<div>
{{- d.include("docs/treeTable/detail/demo.md") }}
</div>

<h2 id="api" lay-toc="{}">API</h2>

| API                                                                                      | 描述                          |
|------------------------------------------------------------------------------------------|-----------------------------|
| var treeTable = layui.treeTable;                                                         | 获得 `treeTable` 模块。          |
| [treeTable.render(options)](#render)                                                     | treeTable 组件渲染，核心方法。        |
| [treeTable.reload(id, options)](#reload)                                                 | treeTable 组件重载。             |
| [treeTable.reloadData(id, options)](#reloadData)                                         | treeTable 组件重载数据。           |
| [treeTable.getData(id, simpleData)](#getData)                                            | 获取树表数据。                     |
| [treeTable.getNodeDataByIndex(id, index)](#getNodeDataByIndex)                           | 通过数据行对应的data-index获取其对应的数据。 |
| [treeTable.updateNode(id, index, newNode)](#updateNode)                                  | 更新行数据。                      |
| [treeTable.removeNode(id, node)](#removeNode)                                            | 删除行记录。                      |
| [treeTable.addNodes(id, parentIndex, index, newNodes, focus)](#addNodes)                 | 新增行记录。                      |
| [treeTable.expandNode(id, index, expandFlag, sonSign, focus, callbackFlag)](#expandNode) | 展开或关闭节点。                    |
| [treeTable.expandAll(id, expandFlag)](#expandAll)                                        | 展开或关闭全部节点（目前只支持关闭全部）。       |
| [treeTable.on](#on)                                                                      | 树表事件，用法同table.on。           |
| [treeTable.eachCols](#eachCols)                                                          | 遍历字段，用法同table.eachCols。     |
| [treeTable.checkNode(id, node, checked, callbackFlag)](#checkNode)                       | 勾选或取消勾选单个节点。                |
| [treeTable.checkAllNodes(id, checked)](#checkAllNodes)                                   | 勾选所有或取消所有节点。                |

<h2 id="render" lay-toc="{level: 2}">渲染</h2>

`treeTable.render(options);`

- 参数 `options` : 基础属性配置项。[#详见属性](#options)

<h2 id="options" lay-toc="{level: 2, hot: true}">属性</h2>

<div>
{{- d.include("docs/treeTable/detail/options.md") }}
</div>

<h2 id="reload" lay-toc="{level: 2, hot: true}">重载</h2>
`treeTable.reload(id, options);` 用法同table.reload

- 参数 `id` : 表格实例id
- 参数 `options` : 基础属性配置项。[#详见属性](#options)

<h2 id="reloadData" lay-toc="{level: 2, hot: true}">重载数据</h2>
`treeTable.reloadData(id, options);` 用法同table.reloadData

- 参数 `id` : 表格实例id
- 参数 `options` : 只支持跟数据相关的参数

<h2 id="getData" lay-toc="{level: 2, hot: true}">获取树表数据</h2>
`treeTable.getData(id, simpleData);`

- 参数 `id` : 表格实例id
- 参数 `simpleData` : 是否为简单数据，为true时返回简单数据结构的数据，否则则为带层级的数据

<h2 id="getNodeDataByIndex" lay-toc="{level: 2, hot: true}">获取树表对应下标的数据</h2>
`treeTable.getNodeDataByIndex(id, index);`

- 参数 `id` : 表格实例id
- 参数 `index` : 节点对应的行下标（data-index）
- 返回 数据对象 返回的是克隆过的对其进行修改不会影响原始数据

<h2 id="updateNode" lay-toc="{level: 2, hot: true}">更新行数据</h2>
`treeTable.updateNode(id, index, newNode);`

- 参数 `id` : 表格实例id
- 参数 `index` : 节点对应的行下标（data-index）
- 参数 `newNode` : 新数据

<h2 id="removeNode" lay-toc="{level: 2, hot: true}">删除行记录</h2>
`treeTable.removeNode(id, node);`

- 参数 `id` : 表格实例id
- 参数 `node` : 要删除的节点数据，可以是dataIndex（节点上的lay-data-index）或者节点数据对象

<h2 id="addNodes" lay-toc="{level: 2, hot: true}">新增行数据</h2>
`treeTable.addNodes(id, parentIndex, index, newNodes, focus);`

- 参数 `id` : 表格实例id
- 参数 `parentIndex` : 父节点数据下标
- 参数 `index` : 数据插入的下标，默认空或者null或者-1为插入到最后，否则则插入到对应下标
- 参数 `newNodes` : 新增的节点，如果新增的是多个节点用数组的形式，如果只有一个节点也可以是对象的形式
- 参数 `focus` : 是否聚焦到新增的节点，默认不聚焦，如果存在多个则聚焦到第一个新增的节点
- 返回 新增数据，不管是新增一个还是多个都返回一个数组

<h2 id="expandNode" lay-toc="{level: 2, hot: true}">展开或关闭节点</h2>
`treeTable.expandNode(id, index, expandFlag, sonSign, callbackFlag);`

- 参数 `id` : 表格实例id
- 参数 `index` : 展开行的dataIndex
- 参数 `expandFlag` : 折叠状态，true: 展开,false: 关闭,null: 切换
- 参数 `sonSign` : 子节点是否做同样的操作，只有expandFlag为true/false该参数才会起作用，如果是切换的话即使该参数为true也不会生效
- 参数 `callbackFlag` : 是否触发事件（beforeExpand和onExpand）
- 返回 如果操作的节点不是一个父节点则返回null，否则返回操作之后的折叠状态

<h2 id="expandAll" lay-toc="{level: 2, hot: true}">展开或关闭全部节点（目前只支持关闭全部）</h2>
`treeTable.expandAll(id, expandFlag);`

- 参数 `id` : 表格实例id
- 参数 `expandFlag` : 折叠状态，true: 展开,false: 关闭

<h2 id="checkNode" lay-toc="{level: 2, hot: true}">勾选或取消勾选单个节点</h2>
`treeTable.checkNode(id, node, checked, callbackFlag);`

- 参数 `id` : 表格实例id
- 参数 `node` : 操作的节点数据或dataIndex
- 参数 `checked` : 勾选状态，true: 选中, false: 取消选中, null:
  切换，调用不分单选多选会根据表格中存在radio列或者checkbox决定，radio不支持切换只能明确传入真假值并且如果callbackFlag为真，checked:
  false会被忽略
- 参数 `callbackFlag` : 是否触发事件，对应的事件跟table的radio和checkbox事件用法一样

<h2 id="checkAllNodes" lay-toc="{level: 2, hot: true}">勾选所有或取消所有节点</h2>
`treeTable.checkAllNodes(id, checked);`

- 参数 `id` : 表格实例id
- 参数 `checked` : 勾选状态，true: 选中, false: 取消选中, null: 复选模式支持切换，单选不支持空值参数


