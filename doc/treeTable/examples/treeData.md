<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>treeTable 简单使用 - Layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{= d.layui.cdn.css }}" rel="stylesheet">
</head>
<body>
<!-- 
本「简单使用」包含：将树形的data渲染成树表。
-->
 
<div style="padding: 16px;"> 
  <table class="layui-hide" id="test" lay-filter="test"></table>
</div>
 
<script src="{{= d.layui.cdn.js }}"></script>
<script>

</script>

</body>
</html>
